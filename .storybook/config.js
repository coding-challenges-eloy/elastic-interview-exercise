import { configure } from "@storybook/react";
// we need this to create jest snapshots from storybook stories
import requireContext from "require-context.macro";
// load Elastic UI theme
import "!style-loader!css-loader!sass-loader!./scss-loader.scss";

// automatically import all files ending in *.stories.js
const req = requireContext("../src", true, /\.stories\.js$/);
function loadStories() {
  req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);
