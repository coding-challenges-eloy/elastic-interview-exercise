# Cloud- Principal UI Engineer at Elastic - Stack Overflow

URL: https://stackoverflow.com/jobs/273661/cloud-principal-ui-engineer-elastic

## Job description

At Elastic, we have a simple goal: to solve the world's data problems with products that delight and inspire. As the company behind the popular open source projects — Elasticsearch, Kibana, Logstash, and Beats — we help people around the world do great things with their data. From stock quotes to Twitter streams, Apache logs to WordPress blogs, our products are extending what's possible with data, delivering on the promise that good things come from connecting the dots. Diversity drives our vibe. We unite Elasticians employees across 30+ countries into one coherent team, while the broader community spans across over 100 countries.

**About The Role:**

You will be responsible for technical design and work in the User Journey area of Elastic’s Cloud product. The User Journey area is a cross-functional team responsible for trial user flow, UX, design and development of Elastic’s Cloud user portal, integrations with 3rd party hosting providers, and more. You will lead by example and should participate in coding, debugging complex failure scenarios, and triaging bugs. You’ll analyze the current system, it’s strength and limitations. You will participate in roadmap and project planning efforts and will have ownership for delivering it. You’ll be participating in project management efforts as the teams execute on plans, and you’ll have a role in communicating progress and status to stakeholders.

**Engineering Philosophy:**

Engineering a highly complex distributed system that is easy to operate via elegantly designed components and APIs is a non-trivial effort. It requires solid software development skills, and more meaningfully, a sharp mind and the ability to think like a user. We care deeply about giving you full ownership of what you’re working on. Our company fundamentally believes phenomenal minds achieve greatness when they are set free and are surrounded and challenged by their peers, which is clearly visible in our organization. We feel that anyone needs to be in a position to comment on truly anything, regardless of his or her role within the company. Does this sound like you? Then you should click apply!

**Some of the things you'll work on:**

- Work with a distributed team of engineers from all across the globe.
- Lead our efforts to measure, monitor, and improve our platform performance. This includes things like: How do we measure changes in performance behavior each time we release? What are our key areas that contribute to production issues? How do we identify and address them?
- Craft end-to-end modules making it easy for users to connect and visualize data within minutes. You will collaborate with the Elasticsearch and the Kibana teams to build dashboards, define data schema, new transformation and processors, etc.
- Own, curate, and execute the Cloud User Journey area product roadmap, collaborating with product managers, engineering managers, and other stakeholders across Cloud and Elastic teams.
- Understand our company strategy and help to translate it into technical deliverables and guide Cloud’s product direction to realize it.
- Create technical designs and build POCs for new efforts, validating a wild idea works before contributing to it.
- Be a contact point in Cloud for other teams within Elastic. Examples include helping Support with difficult cases or consulting the Elastic Stack engineers with designing new features in a Cloud compatible way.
- Be hands-on with the codebase. Review work done by the team, and provide constructive feedback.
- Help the team define coding practices and standards.

**What you will bring along:**

- A track record of successfully leading multi-functional teams to deliver quality content.
- Previous experience working with stakeholders outside of Engineering.
- You have 5-7 years experience in a hands-on Javascript programming role.
- Comfortable working and communicating with teams from several functions – design, data analytics, and distributed engineering.
- You have worked on a SAAS platform or product.
- Previous experience in an ownership role for roadmap curation and execution.
- Experience integrating with marketplace and/or console offers from hosting providers (Azure, GCP, etc.) is a huge plus.
- A self starter who with experience working across multiple technical teams and decision makers

**Additional Information:**

- Deeply competitive pay and benefits
- Equity compensation
- Catered lunches, snacks, and beverages in most offices
- An environment in which you can balance great work with a great life
- Passionate people building phenomenal products

Elastic is an Equal Employment employer committed to the principles of equal employment opportunity and affirmative action for all applicants and employees. Qualified applicants will receive consideration for employment without regard to race, color, religion, sex, sexual orientation, gender perception or identity, national origin, age, marital status, protected veteran status, or disability status or any other basis protected by federal, state or local law, ordinance or regulation. Elastic also makes reasonable accommodations for disabled employees consistent with applicable law.
