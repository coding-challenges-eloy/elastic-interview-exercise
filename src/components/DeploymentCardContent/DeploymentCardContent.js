import React from "react";
import {
  EuiBadge,
  EuiHorizontalRule,
  EuiStat,
  EuiFlexItem,
  EuiFlexGroup,
  EuiListGroup,
  EuiListGroupItem
} from "@elastic/eui";

export const DeploymentCardContent = ({
  displayId,
  kibana,
  monitoring,
  plan: { availabilityZones, instanceCapacity, instanceCount, version },
  regionId
}) => (
  <>
    <EuiBadge iconType="number">{displayId}</EuiBadge>
    <EuiBadge iconType="mapMarker">{regionId}</EuiBadge>
    <EuiHorizontalRule margin="m" />
    <EuiFlexGroup responsive={false}>
      <EuiFlexItem>
        <EuiStat title={instanceCapacity} description="RAM" />
      </EuiFlexItem>
      <EuiFlexItem>
        <EuiStat title={version} description="Version" />
      </EuiFlexItem>
      <EuiFlexItem>
        <EuiStat
          textAlign="right"
          title={availabilityZones}
          description="Zones"
        />
      </EuiFlexItem>
      <EuiFlexItem>
        <EuiStat textAlign="right" title={instanceCount} description="Nodes" />
      </EuiFlexItem>
    </EuiFlexGroup>
    <EuiHorizontalRule margin="xs" />
    <EuiListGroup flush={true}>
      {kibana && kibana.enabled && (
        <EuiListGroupItem iconType="logoKibana" label="Kibana enabled" />
      )}
      {monitoring && monitoring.enabled && (
        <EuiListGroupItem iconType="monitoringApp" label="Monitoring enabled" />
      )}
    </EuiListGroup>
  </>
);
