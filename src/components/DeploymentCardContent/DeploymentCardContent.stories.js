import React from "react";

import { storiesOf } from "@storybook/react";

import { DeploymentCardContent } from "./DeploymentCardContent";

const MOCK_PROPS = {
  displayId: "displayId",
  kibana: { enabled: true },
  monitoring: { enabled: true },
  plan: {
    availabilityZones: 2,
    instanceCapacity: 4096,
    instanceCount: 4,
    version: "1.0.0"
  },
  regionId: "regionId"
};

storiesOf("Deployments/Card/Content", module)
  .add("default", () => <DeploymentCardContent {...MOCK_PROPS} />)
  .add("without kibana", () => (
    <DeploymentCardContent {...MOCK_PROPS} kibana={false} />
  ))
  .add("without monitoring", () => (
    <DeploymentCardContent {...MOCK_PROPS} monitoring={false} />
  ));
