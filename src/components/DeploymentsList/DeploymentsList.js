import React from "react";
import { EuiFlexGroup, EuiFlexItem } from "@elastic/eui";
import { DeploymentCard } from "components/DeploymentCard";

export const DeploymentsList = ({ deployments }) =>
  deployments &&
  deployments.length && (
    <EuiFlexGroup justifyContent="spaceBetween" wrap>
      {deployments.map((deployment, i) => (
        <EuiFlexItem key={i} style={{ minWidth: "360px" }}>
          <DeploymentCard {...deployment} />
        </EuiFlexItem>
      ))}
    </EuiFlexGroup>
  );
