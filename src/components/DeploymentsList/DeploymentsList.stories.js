import React from "react";

import { storiesOf } from "@storybook/react";

import { DeploymentsList } from "./DeploymentsList";
import DEPLOYMENTS_MOCK from "../../deployments.json";

storiesOf("Deployments", module).add("List", () => (
  <DeploymentsList deployments={DEPLOYMENTS_MOCK.record.slice(0, 9)} />
));
