import React, { useEffect, useState } from "react";
import {
  EuiFieldSearch,
  EuiFlexGroup,
  EuiFlexItem,
  EuiLoadingSpinner,
  EuiPageBody,
  EuiPageContent,
  EuiPageContentBody,
  EuiPageContentHeaderSection,
  EuiPageContentHeader,
  EuiSpacer,
  EuiText,
  EuiTitle
} from "@elastic/eui";
import { DeploymentsList } from "components/DeploymentsList";
import { fetchDeployments } from "./effects";

const handleChange = cb => e => cb(e.target.value);

export const DeploymentsPage = () => {
  const [data, setData] = useState({ isSearchig: true, record: [] });

  useEffect(fetchDeployments(setData), []);

  const [filter, setFilter] = useState("");

  const deployments = data.record.filter(({ displayName }) =>
    displayName.match(new RegExp(filter, "i"))
  );

  return (
    <EuiPageBody>
      <EuiPageContent>
        <EuiPageContentHeader>
          <EuiPageContentHeaderSection>
            <EuiTitle>
              <h2>Deployments</h2>
            </EuiTitle>
          </EuiPageContentHeaderSection>
        </EuiPageContentHeader>
        <EuiPageContentBody>
          <EuiFlexGroup alignItems="baseline">
            <EuiFlexItem>
              <EuiFieldSearch
                placeholder="Search for deployment name"
                value={filter}
                onChange={handleChange(setFilter)}
                aria-label="Search for deployment name"
              />
            </EuiFlexItem>
            <EuiFlexItem>
              {data.isSearchig ? (
                <EuiLoadingSpinner />
              ) : (
                <EuiText textAlign="right">
                  Displaying {deployments.length} of {data.record.length} total
                  deployments.
                </EuiText>
              )}
            </EuiFlexItem>
          </EuiFlexGroup>
          <EuiSpacer />
          {!data.isSearching && deployments && !!deployments.length && (
            <DeploymentsList deployments={deployments} />
          )}
        </EuiPageContentBody>
      </EuiPageContent>
    </EuiPageBody>
  );
};
