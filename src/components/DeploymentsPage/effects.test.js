import { fetchData } from "./effects";

test("call callback with results", () => {
  // mocks
  const cbSpy = jest.fn();
  global.fetch = jest.fn(() =>
    Promise.resolve(
      new global.Response(
        new Blob([JSON.stringify({ hello: "world" })], {
          type: "application/json"
        })
      )
    )
  );

  fetchData(cbSpy);

  // assertion
  setTimeout(() => {
    expect(cbSpy).toHaveBeenCalledWith({ hello: "world" });
  }, 0);
});
