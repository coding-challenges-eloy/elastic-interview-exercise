export async function fetchData(cb) {
  const result = await fetch("http://localhost:3000/deployments.json").then(
    res => res.json()
  );

  cb(result);
}

export const fetchDeployments = cb => () => {
  fetchData(cb);
};
