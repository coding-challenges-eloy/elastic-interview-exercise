import React from "react";

import { storiesOf } from "@storybook/react";

import { DeploymentsPage } from "./DeploymentsPage";
import DEPLOYMENTS_MOCK from "../../deployments.json";

storiesOf("Deployments", module).add("Page", () => (
  <DeploymentsPage
    data={{
      isSearching: false,
      totalCount: 9,
      record: DEPLOYMENTS_MOCK.record.slice(0, 9)
    }}
  />
));
