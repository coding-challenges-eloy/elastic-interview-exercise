import React from "react";
import { EuiTitle, EuiHealth } from "@elastic/eui";

export const DeploymentCardHead = ({ healthy, title }) => (
  <EuiTitle size="s">
    <h2>
      <EuiHealth color={healthy ? "success" : "danger"} /> {title}
    </h2>
  </EuiTitle>
);
