import React from "react";

import { storiesOf } from "@storybook/react";

import { DeploymentCardHead } from "./DeploymentCardHead";

storiesOf("Deployments/Card/Heading", module)
  .add("healthy", () => (
    <DeploymentCardHead healthy={true} title="Deplyemnt title" />
  ))
  .add("not healthy", () => (
    <DeploymentCardHead healthy={false} title="Deplyemnt title" />
  ));
