import React from "react";
import { EuiPanel } from "@elastic/eui";
import { DeploymentCardContent } from "components/DeploymentCardContent";
import { DeploymentCardHead } from "components/DeploymentCardHead";

export const DeploymentCard = props => (
  <EuiPanel>
    <DeploymentCardHead healthy={props.healty} title={props.displayName} />
    <DeploymentCardContent
      displayId={props.displayId}
      instanceCapacity={props.plan.instanceCapacity}
      kibana={props.kibana}
      monitoring={props.monitoring}
      plan={props.plan}
      regionId={props.regionId}
    />
  </EuiPanel>
);
