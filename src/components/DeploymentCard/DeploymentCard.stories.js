import React from "react";

import { storiesOf } from "@storybook/react";

import { DeploymentCard } from "./DeploymentCard";
import DEPLOYMENT_MOCK from "./deployment.mock.json";

storiesOf("Deployments", module).add("Card", () => (
  <DeploymentCard {...DEPLOYMENT_MOCK} />
));
